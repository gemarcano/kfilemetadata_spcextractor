// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
#ifndef MARCANO_SPCEXTRACTOR_H_
#define MARCANO_SPCEXTRACTOR_H_
#include <KFileMetaData/ExtractionResult>
#include <KFileMetaData/ExtractorPlugin>
#include <spc_tag/spc_tag.h>

#include <iostream>
#include <filesystem>
#include <type_traits>

namespace marcano
{
	/** Helper class, wraps around the libscp_tag functionality.
	 */
	class spc_tag
	{
	public:
		/** Constructor. Parses out the metadata of the SPC file at the path
		 * provided, and sets the elements of spc_tag_metadata accordingly. See
		 * libspc_tag for details about the elements of spc_tag_metadata.
		 *
		 * @param[in] path Path to the SPC file to parse.
		 */
		spc_tag(const std::filesystem::path& path);

		/** Destructor.
		 *
		 * Releases resources held by the data spc_tag_metadata variable.
		 */
		~spc_tag();

		/** This variable holds the metadata of the parsed SPC file.
		 */
		spc_tag_metadata *data;
	};

	/** KFileMetadata SPC data extractor class.
	 */
	class SpcExtractor : public KFileMetaData::ExtractorPlugin
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.kde.kf5.kfilemetadata.ExtractorPlugin" FILE "spcextractor.json")
		Q_INTERFACES(KFileMetaData::ExtractorPlugin)

		public:
		/** Constructor.
		 */
		explicit SpcExtractor(QObject *parent = nullptr);

		/** Extract metadata into provided ExtractionResult.
		 *
		 * This extractor looks for metadata in both the id666 and xid666 tags.
		 * If there are duplicate tags (xid666 tags that mirror the id666
		 * ones), it prioritizes the xid666 over the id666 ones. It sets the
		 * type of the media to Audio. The following is a list of Properties it
		 * searches for, and from where it gets the data:
		 *  - Channels:     2 (SNES has 2 audio channels)
		 *  - Title:        xid666 0x01 element, or xid666 0x10 element, or
		 *                  id666 title field
		 *  - Album:        xid666 0x02 element, or id666 game field
		 *  - Composer:     xid666 0x03 element, or id666 performer field
		 *  - CreationDate: xid666 0x05 element, or id666 date field
		 *  - Generator:    xid666 0x06 element, or id666 emulator field
		 *  - Comment:      xid666 0x07 element, or id666 comment field
		 *  - TrackNumber:  xid666 0x12 element
		 *  - Publisher:    xid666 0x13 element
		 *  - Duration:     Calculated from xid666 elements 0x30 through 0x34,
		 *                  or id666 number of seconds to play
		 *
		 * @param[out] result ExtractionResult to fill with metadata.
		 *
		 * @post ExtractionResult is filled with metadata found. If the
		 *  metadata isn't found or is empty, it is not added to the result.
		 */
		void extract(KFileMetaData::ExtractionResult *result) override;

		/** List supporetd mimetypes.
		 *
		 * This extractor only supports audio/x-spc.
		 *
		 * @returns A list with only "audio/x-spc" in it.
		 */
		QStringList mimetypes() const override;
	};
}
#endif//MARCANO_SPCEXTRACTOR_H_
