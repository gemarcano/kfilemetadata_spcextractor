// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
#include "spcextractor.h"

#include <KFileMetaData/ExtractionResult>
#include <KFileMetaData/ExtractorPlugin>
#include <spc_tag/spc_tag.h>

#include <iostream>
#include <fstream>
#include <filesystem>
#include <type_traits>
#include <map>

namespace marcano
{
	template <class Accessor>
	spc_tag_accessor get_spc_tag_accessor(Accessor& acc)
	{
		auto read_ = [](void* data, void *src, size_t amount) -> size_t
		{
			return reinterpret_cast<Accessor*>(data)->read(src, amount);
		};

		auto write_ = [](void* data, const void *dest, size_t amount) -> size_t
		{
			return reinterpret_cast<Accessor*>(data)->write(dest, amount);
		};

		auto seek_ = [](void *data, long pos, int flag) -> int
		{
			return reinterpret_cast<Accessor*>(data)->seek(pos, flag);
		};
		return spc_tag_accessor{read_, write_, seek_, &acc};
	}

	template <class Stream>
	class istream_accessor
	{
	public:
		istream_accessor(Stream&& is)
			:is_(std::move(is))
		{}

		size_t read(void *src, size_t amount)
		{
			is_.read(reinterpret_cast<char*>(src), amount);
			return is_.gcount();
		}

		size_t write(const void*, size_t)
		{
			return 0;
		}

		int seek(long pos, int flag)
		{
			auto get_flag = [](int f)
			{
				switch(f)
				{
					case SEEK_CUR: return std::ios::cur;
					case SEEK_END: return std::ios::end;
					case SEEK_SET: return std::ios::beg;
					default: return std::ios::beg;
				}
			};

			is_.seekg(pos, get_flag(flag));
			return is_ ? 0 : -1;
		}

	private:
		Stream is_;
	};

	spc_tag::spc_tag(const std::filesystem::path& path)
		:data{}
	{
		std::ifstream file(path.string(), std::ios::binary);
		istream_accessor accc(std::move(file));
		spc_tag_accessor acc = get_spc_tag_accessor(accc);
		data = spc_tag_metadata_new();
		spc_tag_metadata_parse(&acc, data);
	}

	spc_tag::~spc_tag()
	{
		spc_tag_metadata_free(data);
	}

	SpcExtractor::SpcExtractor(QObject *parent)
	:KFileMetaData::ExtractorPlugin(parent)
	{}

	void SpcExtractor::extract(KFileMetaData::ExtractionResult *result)
	{
		marcano::spc_tag tag(result->inputUrl().toUtf8().constData());
		result->addType(KFileMetaData::Type::Audio);

		// SNES supports two channels, although not every game is configured to
		// use them
		result->add(KFileMetaData::Property::Channels, 2);

		// Don't bother continuing if we don't have an id666 tag
		if (!spc_tag_metadata_get_has_id666(tag.data))
			return;

		auto which_emulator = [](char emu_ch) -> const char*
		{
			if (emu_ch == '1') return "ZSNES";
			else if (emu_ch == '2') return "Snes9x";
			else return nullptr;
		};

		// Have xid666 take priority, if they exist
		std::map<KFileMetaData::Property::Property, QVariant> mapping;
		spc_tag_xid666 *xid666 = spc_tag_metadata_get_xid666(tag.data);
		if (spc_tag_xid666_get_chunks(xid666))
		{
			int32_t song_ticks = 0;
			for (size_t i = 0; i < spc_tag_xid666_get_chunks(xid666); ++i)
			{
				const spc_tag_xid666_data *iter = spc_tag_xid666_get_data(xid666, i);
				char *data_ = reinterpret_cast<char*>(spc_tag_xid666_data_get_data(iter));
				uint16_t header_data = spc_tag_xid666_data_get_header_data(iter);
				switch (spc_tag_xid666_data_get_id(iter))
				{
				case 0x01:
					mapping.try_emplace(KFileMetaData::Property::Title, data_);
					break;
				case 0x02:
					mapping.try_emplace(KFileMetaData::Property::Album, data_);
					break;
				case 0x03:
					mapping.try_emplace(KFileMetaData::Property::Composer, data_);
					break;
				case 0x04:
					// Dumper
					//mapping.try_emplace(KFileMetaData::Property::Title, iter->data);
					break;
				case 0x05:
				{
					unsigned char *date = reinterpret_cast<unsigned char*>(data_);
					int year = date[0] | (date[1] << 8);
					int month = date[2];
					int day = date[3];
					QDate ddate(year, month, day);
					mapping.try_emplace(KFileMetaData::Property::CreationDate, ddate);
					break;
				}
				case 0x06:
				{
					const char* emulator = which_emulator((char)(header_data));
					if (emulator)
						mapping.try_emplace(KFileMetaData::Property::Generator, emulator);
					break;
				}
				case 0x07:
					mapping.try_emplace(KFileMetaData::Property::Comment, data_);
					break;
				case 0x10:
					// This is the official name
					mapping.try_emplace(KFileMetaData::Property::Title, data_);
					break;
				case 0x11:
					// OST disk
					break;
				case 0x12:
				{
					unsigned track = header_data >> 8;
					mapping.try_emplace(KFileMetaData::Property::TrackNumber, track);
					break;
				}
				case 0x13:
					mapping.try_emplace(KFileMetaData::Property::Publisher, data_);
					break;
				case 0x14:
				{
					unsigned year = header_data;
					char copyright[7];
					snprintf(copyright, sizeof(copyright), "%u", year);
					mapping.try_emplace(KFileMetaData::Property::Copyright, copyright);
				}
				break;
				case 0x30:
				case 0x31:
				case 0x32:
				case 0x33:
				{
					unsigned char *tick_data = reinterpret_cast<unsigned char*>(data_);
					song_ticks += tick_data[0] | tick_data[1] << 8 | tick_data[2] << 16 | tick_data[3] << 24;
				}
				break;

				default: //Unknown, or unimplemented, can't really parse them...
				break;

				}
			}
			if (song_ticks)
				mapping.try_emplace(KFileMetaData::Property::Duration, song_ticks/64000);
		}

		if (*spc_tag_metadata_get_song_title(tag.data) != '\0')
			mapping.try_emplace(KFileMetaData::Property::Title, QString(spc_tag_metadata_get_song_title(tag.data)));
		if (*spc_tag_metadata_get_game_title(tag.data) != '\0')
			mapping.try_emplace(KFileMetaData::Property::Album, QString(spc_tag_metadata_get_game_title(tag.data)));
		int seconds = spc_tag_metadata_get_seconds_until_fade(tag.data) + spc_tag_metadata_get_fade_length_ms(tag.data)/1000;
		if (seconds)
			mapping.try_emplace(KFileMetaData::Property::Duration, seconds);
		if (*spc_tag_metadata_get_song_artist(tag.data) != '\0')
			mapping.try_emplace(KFileMetaData::Property::Composer, spc_tag_metadata_get_song_artist(tag.data));
		if (*spc_tag_metadata_get_comments(tag.data) != '\0')
			mapping.try_emplace(KFileMetaData::Property::Comment, spc_tag_metadata_get_comments(tag.data));

		const char* emulator = which_emulator(spc_tag_metadata_get_emulator_used_to_dump(tag.data));
		if (emulator)
			mapping.try_emplace(KFileMetaData::Property::Generator, emulator);
		const auto& dump_date = spc_tag_metadata_get_dump_date(tag.data);
		if (dump_date.tm_mday != 0)
		{
			QDate ddate(dump_date.tm_year + 1900, dump_date.tm_mon + 1, 1);
			mapping.try_emplace(KFileMetaData::Property::CreationDate, ddate);
		}

		for (auto& pair : mapping)
		{
			result->add(pair.first, pair.second);
		}
	}

	QStringList SpcExtractor::mimetypes() const
	{
		return QStringList{"audio/x-spc"};
	}
}
