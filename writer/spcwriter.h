// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
#ifndef MARCANO_SPCWRITER_H_
#define MARCANO_SPCWRITER_H_
#include <KFileMetaData/WriteData>
#include <KFileMetaData/WriterPlugin>
#include <spc_tag/spc_tag.h>

#include <iostream>
#include <filesystem>
#include <type_traits>

namespace marcano
{
	/** Helper class, wraps around the libscp_tag functionality.
	 */
	class spc_tag
	{
	public:
		/** Constructor. Parses out the metadata of the SPC file at the path
		 * provided, and sets the elements of spc_tag_metadata accordingly. See
		 * libspc_tag for details about the elements of spc_tag_metadata.
		 *
		 * @param[in] path Path to the SPC file to parse.
		 */
		spc_tag(const std::filesystem::path& path);

		/** Destructor.
		 *
		 * Releases resources held by the data spc_tag_metadata variable.
		 */
		~spc_tag();

		/** This variable holds the metadata of the parsed SPC file.
		 */
		spc_tag_metadata *data;
	};

	class SpcWriter : public KFileMetaData::WriterPlugin
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.kde.kf5.kfilemetadata.WriterPlugin" FILE "spcwriter.json")
		Q_INTERFACES(KFileMetaData::WriterPlugin)
	public:
		explicit SpcWriter(QObject *parent = nullptr);
		~SpcWriter();
		QStringList writeMimetypes() const override;
		void write(const KFileMetaData::WriteData& data) override;
	};
}
#endif//MARCANO_SPCWRITER_H_
