// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
#include "spcwriter.h"

#include <KFileMetaData/WriterPlugin>
#include <QDate>
#include <spc_tag/spc_tag.h>

#include <filesystem>
#include <type_traits>
#include <string_view>
#include <map>

namespace marcano
{
	spc_tag::spc_tag(const std::filesystem::path& path)
		:data{}
	{
		FILE* file = std::fopen(path.string().c_str(), "rb");
		data = spc_tag_metadata_new();
		spc_tag_metadata_parse_file(file, data);
		fclose(file);
	}

	spc_tag::~spc_tag()
	{
		spc_tag_metadata_free(data);
	}

	SpcWriter::SpcWriter(QObject *parent)
		:WriterPlugin(parent)
	{}

	SpcWriter::~SpcWriter()
	{}

	QStringList SpcWriter::writeMimetypes() const
	{
		return QStringList{"audio/x-spc"};
	}

	spc_tag_xid666_data* find_xid666(uint8_t type, spc_tag_xid666 *chunk)
	{
		for (size_t i = 0; i < spc_tag_xid666_get_chunks(chunk); ++i)
		{
			spc_tag_xid666_data *iter = spc_tag_xid666_get_data(chunk, i);
			if (spc_tag_xid666_data_get_type(iter) == type)
				return iter;
		}
		return nullptr;
	}

	void SpcWriter::write(const KFileMetaData::WriteData& data)
	{
		const QString url = data.inputUrl();
		const KFileMetaData::PropertyMap properties = data.getAllProperties();

		marcano::spc_tag tag(url.toUtf8().constData());
		spc_tag_metadata *metadata = tag.data;

		if (properties.size())
			spc_tag_metadata_set_has_id666(metadata, true);

		auto prepare_string = [&properties, &metadata](uint8_t id, char* id666_data, size_t id666_data_size){
			auto *subchunk = find_xid666(id, spc_tag_metadata_get_xid666(metadata));
			const auto &qstring = properties[KFileMetaData::Property::Album].toString();
			std::string_view string(qstring.toUtf8().constData(), std::min(qstring.size(), 255));
			const unsigned char* raw_data = reinterpret_cast<const unsigned char*>(string.data());

			if (!subchunk)
			{
				subchunk = spc_tag_xid666_add_data(spc_tag_metadata_get_xid666(metadata), id, string.size()+1, raw_data);
			}
			else
			{
				spc_tag_xid666_data_update(subchunk, id, string.size()+1, raw_data);
			}
			// Update ID666 also, for as many characters as will fit
			if (id666_data)
				snprintf(id666_data, id666_data_size, "%s", string.data());
		};

		auto prepare_integer = [&properties, &metadata](uint8_t id)
		{
			auto *subchunk = find_xid666(id, spc_tag_metadata_get_xid666(metadata));
			unsigned integer = properties[KFileMetaData::Property::Album].toUInt();
			unsigned char raw_data[4];
			raw_data[3] = integer >> 24;
			raw_data[2] = integer >> 16;
			raw_data[1] = integer >> 8;
			raw_data[0] = integer;

			if (!subchunk)
			{
				subchunk = spc_tag_xid666_add_data(spc_tag_metadata_get_xid666(metadata), id, 4, raw_data);
			}
			else
			{
				spc_tag_xid666_data_update(subchunk, id, 4, raw_data);
			}
			return integer;
		};

		if (properties.contains(KFileMetaData::Property::Title))
		{
			char tmp[33];
			prepare_string(0x01, tmp, 33);
			spc_tag_metadata_set_song_title(metadata, tmp, 32);
		}

		if (properties.contains(KFileMetaData::Property::Album))
		{
			char tmp[33];
			prepare_string(0x02, tmp, 33);
			spc_tag_metadata_set_game_title(metadata, tmp, 32);
		}

		if (properties.contains(KFileMetaData::Property::Composer))
		{
			char tmp[33];
			prepare_string(0x3, tmp, 33);
			spc_tag_metadata_set_song_artist(metadata, tmp, 32);
		}

		if (properties.contains(KFileMetaData::Property::CreationDate))
		{
			QDate date = properties[KFileMetaData::Property::CreationDate].toDate();
		}

		if (properties.contains(KFileMetaData::Property::Generator))
		{

		}

		if (properties.contains(KFileMetaData::Property::Comment))
		{
			char tmp[33];
			prepare_string(0x7, tmp, 33);
			spc_tag_metadata_set_comments(metadata, tmp, 32);
		}

		if (properties.contains(KFileMetaData::Property::TrackNumber))
		{

		}

		if (properties.contains(KFileMetaData::Property::Publisher))
		{
			prepare_string(0x13, nullptr, 0);
		}

		if (properties.contains(KFileMetaData::Property::Copyright))
		{
			// This one is going to be tricky-- we can only store a year...
		}

		if (properties.contains(KFileMetaData::Property::Duration))
		{
			// Fun. This one used to be multiple elements, now bundled together...
		}

		if (properties.contains(KFileMetaData::Property::CreationDate))
		{
			QDate date = properties[KFileMetaData::Property::CreationDate].toDate();
		}
	}
}
